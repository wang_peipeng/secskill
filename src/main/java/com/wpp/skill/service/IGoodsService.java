package com.wpp.skill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.skill.pojo.Goods;
import com.wpp.skill.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 * 商品表 服务类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface IGoodsService extends IService<Goods> {
    List<GoodsVo> fidGoods();
    /**
     * 根据商品id获取商品数据
     */
    GoodsVo  findGoodsVOById(Long goodsId);
}
