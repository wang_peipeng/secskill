package com.wpp.skill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.skill.pojo.SeckillGoods;

/**
 * <p>
 * 秒杀商品表 服务类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
