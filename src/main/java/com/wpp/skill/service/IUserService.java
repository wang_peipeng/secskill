package com.wpp.skill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.skill.pojo.User;
import com.wpp.skill.vo.LoginVo;
import com.wpp.skill.vo.ResBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author wpp
 * @since 2022-07-30
 */
public interface IUserService extends IService<User> {
   ResBean doLogin(LoginVo loginVo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 根据ticket获取用户
     * @param userTicket cookie键值
     * @return
     */
   User getUserByCookie(String userTicket, HttpServletRequest servletRequest, HttpServletResponse servletResponse);

   ResBean updatePassword(String userTicket,String password, HttpServletRequest request, HttpServletResponse response);
}
