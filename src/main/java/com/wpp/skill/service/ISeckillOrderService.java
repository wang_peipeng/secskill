package com.wpp.skill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.skill.pojo.SeckillOrder;

/**
 * <p>
 * 秒杀订单表 服务类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface ISeckillOrderService extends IService<SeckillOrder> {

}
