package com.wpp.skill.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.skill.exception.GlobalException;
import com.wpp.skill.mapper.UserMapper;
import com.wpp.skill.pojo.User;

import com.wpp.skill.service.IUserService;
import com.wpp.skill.utils.CookieUtil;
import com.wpp.skill.utils.MD5utils;
import com.wpp.skill.utils.UUIDUtil;
import com.wpp.skill.vo.LoginVo;
import com.wpp.skill.vo.ResBean;
import com.wpp.skill.vo.ResBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import sun.security.krb5.internal.Ticket;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author wpp
 * @since 2022-07-30
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private  RedisTemplate redisTemplate;
    @Override
    public ResBean doLogin(LoginVo loginVo, HttpServletRequest request,HttpServletResponse response){
        String password = loginVo.getPassword();
        String mobile = loginVo.getMobile();

        User user = userMapper.selectById(mobile);
        if(null==user){
            throw new GlobalException(ResBeanEnum.USER_ERROR);
        }
        if(!user.getPassword().equals(MD5utils.formPassToDbPass(password,user.getSalt()))){
            throw new GlobalException(ResBeanEnum.LOGIN_ERROR);
        }
        //生成cookie
        String ticket = UUIDUtil.uuid();
        //将用户信息存入redis
        redisTemplate.opsForValue().set("user" + ticket,user);
        CookieUtil.setCookie(request,response,"userTicket",ticket);
        //之前放到session中
       //request.getSession().setAttribute(ticket,user);

        return ResBean.success(ticket);
    }

    @Override
    public User getUserByCookie(String userTicket,HttpServletRequest request, HttpServletResponse response) {
        if(StringUtils.isBlank(userTicket)){
            return null;
        }
        User user = (User) redisTemplate.opsForValue().get("user"+ userTicket);
        if(user != null){
            CookieUtil.setCookie(request,response,"userTicket",userTicket);
        }
        return user;
    }

    /**
     * 更新密码
     * @param userTicket
     * @param password
     * @param request
     * @param response
     * @return
     */
    @Override
    public ResBean updatePassword(String userTicket, String password, HttpServletRequest request, HttpServletResponse response) {
        User user = getUserByCookie(userTicket,request,response);
        if(user == null){
            throw new GlobalException(ResBeanEnum.MOBILE_NOT_EXIT);
        }
        user.setPassword(MD5utils.inputPassToDbPass(password,user.getSalt()));
        int result = userMapper.updateById(user);
        if(1 == result){
            redisTemplate.delete("user:"+userTicket);
            return ResBean.success(ResBeanEnum.SUCCESS);
        }
        return ResBean.error(ResBeanEnum.PASSWORD_NOT_EXIT);
    }
}
