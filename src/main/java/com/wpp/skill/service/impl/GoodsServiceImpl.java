package com.wpp.skill.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.skill.mapper.GoodsMapper;
import com.wpp.skill.pojo.Goods;
import com.wpp.skill.service.IGoodsService;
import com.wpp.skill.vo.GoodsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 商品表 服务实现类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
@Service
public class GoodsServiceImpl extends ServiceImpl<GoodsMapper, Goods> implements IGoodsService {

    @Autowired
    private GoodsMapper goodsMapper;

    @Override
    public List<GoodsVo> fidGoods(){
        return goodsMapper.findGoodsVo();
    }

    @Override
    public GoodsVo findGoodsVOById(Long goodsId) {
        return goodsMapper.findGoodsVoById(goodsId);
    }
}
