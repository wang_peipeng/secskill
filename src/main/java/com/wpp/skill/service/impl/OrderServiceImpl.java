package com.wpp.skill.service.impl;
import java.math.BigDecimal;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.UpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wpp.skill.mapper.OrderMapper;
import com.wpp.skill.mapper.SeckillOrderMapper;
import com.wpp.skill.pojo.*;
import com.wpp.skill.service.IGoodsService;
import com.wpp.skill.service.IOrderService;
import com.wpp.skill.service.ISeckillGoodsService;
import com.wpp.skill.vo.GoodsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    ISeckillGoodsService seckillGoodsService;
    @Autowired
    private SeckillOrderMapper seckillOrderMapper;

    @Autowired
    private  IGoodsService goodsService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Transactional
    @Override
    public Order seckill(User user, GoodsVo goods) {
        //秒杀商品库存减一
        SeckillGoods seckillGoods = seckillGoodsService.getOne(new QueryWrapper<SeckillGoods>().eq("goods_id",goods.getId()));
        seckillGoods.setStockCount(seckillGoods.getStockCount()-1);
        //解决超卖问题
       boolean result = seckillGoodsService.update(new UpdateWrapper<SeckillGoods>().set("stock_count",
               seckillGoods.getStockCount()).eq("id",seckillGoods.getGoodsId())
               .gt("stock_count",0));
       if(!result){
           return null;
       }
        //商品库存减一
//        Goods goods1 = goodsService.getOne(new QueryWrapper<Goods>().eq("id",goods.getId()));
//        goods1.setGoodsStock(goods.getGoodsStock()-1);
//        goodsService.updateById(goods);
        //生成订单
        Order order = new Order();
        order.setUserId(user.getId());
        order.setGoodsId(goods.getId());
        order.setDeliveryAddrId(0L);
        order.setGoodsName(goods.getGoodsName());
        order.setGoodsCount(1);
        order.setGoodsPrice(goods.getSeckillPrice());
        order.setOrderChannel(1);
        order.setStatus(0);
        order.setCreateDate(new Date());
        order.setPayDate(new Date());
        orderMapper.insert(order);
        //生成秒杀订单
        SeckillOrder seckillOrder = new SeckillOrder();
        seckillOrder.setUserId(user.getId());
        seckillOrder.setOrderId(order.getId());
        seckillOrder.setGoodsId(goods.getId());
        seckillOrderMapper.insert(seckillOrder);
        redisTemplate.opsForValue().set("order:" + user.getId() + ":" + goods.getId(),seckillOrder);
        return order;
    }
}
