package com.wpp.skill.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wpp.skill.pojo.Order;
import com.wpp.skill.pojo.User;
import com.wpp.skill.vo.GoodsVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface IOrderService extends IService<Order> {

    /**
     * 存储订单
     */
    Order seckill(User user, GoodsVo goods);
}
