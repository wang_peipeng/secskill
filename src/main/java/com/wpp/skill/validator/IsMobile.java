package com.wpp.skill.validator;

import com.wpp.skill.vo.isMobileValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


/**
 * 手机号码判断是否正确工具类
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { isMobileValidator.class})
public @interface IsMobile {

    boolean required() default true;
    String message() default "手机号格式错误";
    Class<?>[] groups() default { };
    Class<? extends Payload>[] payload() default { };
}
