package com.wpp.skill.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResBean {
    private int code;
    private String message;
    private Object obj;
    public static ResBean success(Object object) {
        return new ResBean(ResBeanEnum.SUCCESS.getCode(), ResBeanEnum.SUCCESS.getMessage(), object);
    }
    public static ResBean success(ResBeanEnum resBeanEnum){
        return new ResBean(resBeanEnum.getCode(), resBeanEnum.getMessage(), null);
    }
    public static ResBean success(ResBeanEnum resBeanEnum,Object obj){
        return new ResBean(resBeanEnum.getCode(),resBeanEnum.getMessage(),obj);
    }
    public static ResBean error(ResBeanEnum resBeanEnum){
        return new ResBean(resBeanEnum.getCode(), resBeanEnum.getMessage(), null);
    }
    public static ResBean error(ResBeanEnum resBeanEnum,Object obj){
        return new ResBean(resBeanEnum.getCode(),resBeanEnum.getMessage(),obj);
    }
}
