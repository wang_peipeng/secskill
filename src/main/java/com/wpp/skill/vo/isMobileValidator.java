package com.wpp.skill.vo;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.wpp.skill.utils.ValidatorUtils;
import com.wpp.skill.validator.IsMobile;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class isMobileValidator implements ConstraintValidator<IsMobile,String> {
    private boolean required = false;
    @Override
    public void initialize(IsMobile constraintAnnotation) {
        required = constraintAnnotation.required();
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if(required){
            return ValidatorUtils.validMobile(s);
        }else {
            if(StringUtils.isBlank(s)){
                return true;
            }else {
                return ValidatorUtils.validMobile(s);
            }
        }
    }
}
