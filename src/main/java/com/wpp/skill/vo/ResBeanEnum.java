package com.wpp.skill.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@AllArgsConstructor
public enum ResBeanEnum {
    //通用
    SUCCESS(200,"SUCCESS"),
    ERROR(500,"服务端异常"),
    //登录
    LOGIN_ERROR(500210,"登录名或密码错误"),
    USER_ERROR(500212,"用户不存在"),
    MOBILE_ERROR(500211,"手机号码格式不正确"),
    LOGIN_SUCCESS(200,"登录成功"),
    BIND_ERROR(500212,"参数校验异常"),
    MOBILE_NOT_EXIT(500213,"电话号码不存在"),
    PASSWORD_NOT_EXIT(500213,"密码不存在"),
    //秒杀模块
    EMPTY_STOCK(200500,"库存不足"),
    REPEAT_ERROR(200501,"该商品限购一件");
    private final Integer code;
    private final String message;
}
