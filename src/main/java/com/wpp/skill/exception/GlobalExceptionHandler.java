package com.wpp.skill.exception;

import com.wpp.skill.vo.ResBean;
import com.wpp.skill.vo.ResBeanEnum;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


/**
 * 全局异常处理类
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResBean ExceptionHandler(Exception e){
        if(e instanceof GlobalException){
            GlobalException ex = (GlobalException) e;
            return ResBean.error(ex.getResBeanEnum());
        }else if(e instanceof BindException) {
            BindException ex  = (BindException) e;
            ResBean resBean =  ResBean.error(ResBeanEnum.BIND_ERROR);
            resBean.setMessage("参数异常："+ex.getBindingResult().getAllErrors().get(0).getDefaultMessage());
            return resBean;
        }
        return ResBean.error(ResBeanEnum.ERROR);
    }
}
