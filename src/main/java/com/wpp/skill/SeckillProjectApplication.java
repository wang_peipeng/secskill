package com.wpp.skill;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.wpp.skill.mapper")
public class SeckillProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeckillProjectApplication.class, args);
    }

}
