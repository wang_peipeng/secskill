package com.wpp.skill.controller;

import com.wpp.skill.exception.GlobalException;
import com.wpp.skill.mapper.UserMapper;
import com.wpp.skill.pojo.User;
import com.wpp.skill.service.impl.UserServiceImpl;
import com.wpp.skill.utils.MD5utils;
import com.wpp.skill.vo.LoginVo;
import com.wpp.skill.vo.ResBean;
import com.wpp.skill.vo.ResBeanEnum;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping("/login")
@Slf4j
public class LoginController {

    @Autowired
    UserServiceImpl userService;
    /**
     * 进入登录页面
     * @return
     */
    @RequestMapping(value = "/tologin")
    public String toLogin(){
        return "login";
    }
    @RequestMapping(value = "/doLogin")
    @ResponseBody
    public ResBean doLogin(@Valid LoginVo loginVo, HttpServletRequest request, HttpServletResponse response){
       return userService.doLogin(loginVo,request,response);
    }
}
