package com.wpp.skill.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
@Controller
@RequestMapping("/order")
public class OrderController {

}
