package com.wpp.skill.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 秒杀订单表 前端控制器
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
@Controller
@RequestMapping("/seckill-order")
public class SeckillOrderController {

}
