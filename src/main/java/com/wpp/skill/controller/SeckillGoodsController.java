package com.wpp.skill.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wpp.skill.pojo.Order;
import com.wpp.skill.pojo.SeckillOrder;
import com.wpp.skill.pojo.User;
import com.wpp.skill.service.IGoodsService;
import com.wpp.skill.service.IOrderService;
import com.wpp.skill.service.ISeckillOrderService;
import com.wpp.skill.vo.GoodsVo;
import com.wpp.skill.vo.ResBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

/**
 * <p>
 * 秒杀商品表 前端控制器
 * </p>
 * 优化前QPS：1816.5
 *
 * @author wpp
 * @since 2022-08-13
 */
@Controller
@RequestMapping("/secKill")
public class SeckillGoodsController {

    @Autowired
    private IGoodsService goodsService;
    @Autowired
    private ISeckillOrderService seckillOrderService;

    @Autowired
    private IOrderService orderService;

    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/doSecKill", method = RequestMethod.POST)
    public String doSecKill(Model model, User user, Long goodsId) {

         if(user == null){
           return "login";
         }
        model.addAttribute("user",user);
        GoodsVo goodsVo = goodsService.findGoodsVOById(goodsId);
        if(goodsVo.getStockCount() <=0){
            model.addAttribute("errmsg", ResBeanEnum.EMPTY_STOCK.getMessage());
            return "secKillFail";
        }
        //这句是mybatisplus的语法api
//        SeckillOrder seckillOrder = seckillOrderService.getOne(new QueryWrapper<SeckillOrder>().eq("user_id",user.getId())
//                .eq("goods_id",goodsId));
        //判断是否重复抢购
        SeckillOrder seckillOrder = (SeckillOrder) redisTemplate.opsForValue().get("order:"+user.getId()+":"+goodsId);
        if(seckillOrder != null){
            model.addAttribute("errmsg",ResBeanEnum.REPEAT_ERROR.getMessage());
            return "secKillFail";
        }
        GoodsVo goods = goodsService.findGoodsVOById(goodsId);
        Order order = orderService.seckill(user,goods);
        model.addAttribute("order",order);
        model.addAttribute("goods",goods);
        System.out.println("user:"+user.getId());
        return "orderDetail";
    }
}
