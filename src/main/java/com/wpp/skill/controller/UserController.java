package com.wpp.skill.controller;


import com.wpp.skill.pojo.User;
import com.wpp.skill.rabbitmq.MQSender;
import com.wpp.skill.vo.ResBean;
import com.wpp.skill.vo.ResBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author wpp
 * @since 2022-07-30
 */
@Controller
@RequestMapping("/user")
public class UserController {
    /**
     * 用户信息测试接口
     */
    @RequestMapping("/info")
    @ResponseBody
    public ResBean info(User user){
        System.out.println(user);
        return ResBean.success(ResBeanEnum.SUCCESS,user);
    }

    /**
     * 测试MQ功能
     */
    @Autowired
    private MQSender mqSender;

    @RequestMapping("/mq")
    @ResponseBody
    public  void mq(){
        mqSender.send("Hello MQ");

    }
    @RequestMapping("/mq/send01")
    @ResponseBody
    public void send01(){
        mqSender.send01("direct queue.red");
    }
    @RequestMapping("/mq/send02")
    @ResponseBody
    public  void send02(){
        mqSender.send02("direct queue.blue");

    }

    @RequestMapping("/mq/topic01")
    @ResponseBody
    public void send03(){
        mqSender.send03("topic queue.red");
    }
    @RequestMapping("/mq/topic02")
    @ResponseBody
    public  void send04(){
        mqSender.send04("topic queue.blue.msg");

    }
    @RequestMapping("/mq/topic03")
    @ResponseBody
    public  void send05(){
        mqSender.send05("direct msg.queue.blue");

    }
}
