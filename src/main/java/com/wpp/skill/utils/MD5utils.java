package com.wpp.skill.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

/**
 * MD5加密解密工具
 */
@Component
public class MD5utils {
    public static final String salt="1a2b3c4d";

    public static String md5(String src){
        return DigestUtils.md5Hex(src);
    }
    public static String inputPassToFormPass(String password){
        char a = salt.charAt(1);
        String src = ""+salt.charAt(0)+salt.charAt(1)+password+salt.charAt(2)+salt.charAt(3);
        return md5(src) ;
    }
    public static String formPassToDbPass(String formPass, String salt){
        return md5(""+salt.charAt(0)+salt.charAt(1)+formPass+salt.charAt(2)+salt.charAt(3));
    }
    public static String inputPassToDbPass(String inputPass,String salt){
        String forpass = inputPassToFormPass(inputPass);
        return formPassToDbPass(forpass,salt);
    }

    public static void main(String[] args) {
        System.out.println(inputPassToFormPass("123456"));
        System.out.println(formPassToDbPass("ae6712ae454da07f5a86e2b5b21f4ea2",salt));
        System.out.println(inputPassToDbPass("123456",salt));
    }

}
