package com.wpp.skill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.skill.pojo.Order;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface OrderMapper extends BaseMapper<Order> {

}
