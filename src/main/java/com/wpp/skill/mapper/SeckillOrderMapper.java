package com.wpp.skill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.skill.pojo.SeckillOrder;

/**
 * <p>
 * 秒杀订单表 Mapper 接口
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {

}
