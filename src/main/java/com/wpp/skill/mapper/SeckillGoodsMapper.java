package com.wpp.skill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.skill.pojo.SeckillGoods;

/**
 * <p>
 * 秒杀商品表 Mapper 接口
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface SeckillGoodsMapper extends BaseMapper<SeckillGoods> {

}
