package com.wpp.skill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.skill.pojo.Goods;
import com.wpp.skill.vo.GoodsVo;

import java.util.List;

/**
 * <p>
 * 商品表 Mapper 接口
 * </p>
 *
 * @author wpp
 * @since 2022-08-13
 */
public interface GoodsMapper extends BaseMapper<Goods> {
    /**
     * 查询既是是秒杀的商品
     * @return
     */
    List<GoodsVo> findGoodsVo();

    /**
     * 根据id获取详情
     * @return
     */

    GoodsVo findGoodsVoById(Long goodsId);
}
