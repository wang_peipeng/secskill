package com.wpp.skill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wpp.skill.pojo.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author wpp
 * @since 2022-07-30
 */
public interface UserMapper extends BaseMapper<User> {

}
