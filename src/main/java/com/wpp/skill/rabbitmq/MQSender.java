package com.wpp.skill.rabbitmq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class MQSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    public void send(Object msg){
        log.info("send mq:"+msg);
        rabbitTemplate.convertAndSend("fanoutExchange","",msg);
    }

    public void send01(Object msg){
        log.info("direct 发送："+msg);
        rabbitTemplate.convertAndSend("directExchange","queue.red",msg);
    }
    public void send02(Object msg){
        log.info("direct 发送："+msg);
        rabbitTemplate.convertAndSend("directExchange","queue.blue",msg);
    }

    public void send03(Object msg){
        log.info("topic 发送："+msg);
        rabbitTemplate.convertAndSend("topicExchange","queue.red",msg);
    }
    public void send04(Object msg){
        log.info("topic 发送："+msg);
        rabbitTemplate.convertAndSend("topicExchange","blue.queue.msg.abc",msg);
    }
    public void send05(Object msg){
        log.info("topic 发送："+msg);
        rabbitTemplate.convertAndSend("topicExchange","msg.queue.blue",msg);
    }
}
